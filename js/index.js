function init(){
  var stage = new createjs.Stage('canvas');
  createjs.Touch.enable(stage);
  createjs.MotionGuidePlugin.install();
  var queue = new createjs.LoadQueue();
  queue.installPlugin(createjs.Sound);
  queue.loadManifest(manifest);

  var startView,bg,user,bubble,apple,blueBerry,redBerry;
  var gameRunInterval,downIntervel;
  var score = 0;
  var scoreTxt;
  var bubbles = [],bubbleIndex = -1;
  var xp = [];

  var userSprite = new createjs.SpriteSheet({images: ["images/user.png"], frames: [[0,0,206,214,0,103.1,112.25],[206,0,206,214,0,103.1,112.25],[412,0,206,214,0,103.1,112.25],[618,0,206,214,0,103.1,112.25],[824,0,206,214,0,103.1,112.25],[1030,0,206,214,0,103.1,112.25],[1236,0,206,214,0,103.1,112.25],[1442,0,206,214,0,103.1,112.25],[1648,0,206,214,0,103.1,112.25],[0,214,206,214,0,103.1,112.25],[206,214,206,214,0,103.1,112.25],[412,214,206,214,0,103.1,112.25],[618,214,206,214,0,103.1,112.25],[824,214,206,214,0,103.1,112.25],[1030,214,206,214,0,103.1,112.25],[1236,214,206,214,0,103.1,112.25],[1442,214,206,214,0,103.1,112.25],[1648,214,206,214,0,103.1,112.25],[0,428,206,214,0,103.1,112.25],[0,0,206,214,0,103.1,112.25],[206,428,206,214,0,103.1,112.25],[412,428,206,214,0,103.1,112.25],[618,428,206,214,0,103.1,112.25],[824,428,206,214,0,103.1,112.25],[1030,428,206,214,0,103.1,112.25],[1236,428,206,214,0,103.1,112.25],[1442,428,206,214,0,103.1,112.25],[1648,428,206,214,0,103.1,112.25],[0,642,206,214,0,103.1,112.25],[206,642,206,214,0,103.1,112.25],[412,642,206,214,0,103.1,112.25],[618,642,206,214,0,103.1,112.25],[824,642,206,214,0,103.1,112.25],[1030,642,206,214,0,103.1,112.25],[1236,642,206,214,0,103.1,112.25],[1442,642,206,214,0,103.1,112.25],[1648,642,206,214,0,103.1,112.25],[0,856,206,214,0,103.1,112.25],[206,856,206,214,0,103.1,112.25],[412,856,206,214,0,103.1,112.25],[618,856,206,214,0,103.1,112.25],[824,856,206,214,0,103.1,112.25],[1030,856,206,214,0,103.1,112.25],[1236,856,206,214,0,103.1,112.25],[1442,856,206,214,0,103.1,112.25],[1648,856,206,214,0,103.1,112.25],[206,856,206,214,0,103.1,112.25],[0,1070,206,214,0,103.1,112.25],[206,1070,206,214,0,103.1,112.25],[412,1070,206,214,0,103.1,112.25],[618,1070,206,214,0,103.1,112.25],[824,1070,206,214,0,103.1,112.25],[1030,1070,206,214,0,103.1,112.25],[1236,1070,206,214,0,103.1,112.25],[1442,1070,206,214,0,103.1,112.25],[1648,1070,206,214,0,103.1,112.25],[0,1284,206,214,0,103.1,112.25],[0,1284,206,214,0,103.1,112.25],[0,1284,206,214,0,103.1,112.25],[0,1284,206,214,0,103.1,112.25],[0,1284,206,214,0,103.1,112.25],[0,1284,206,214,0,103.1,112.25],[0,1284,206,214,0,103.1,112.25],[0,1284,206,214,0,103.1,112.25],[0,1284,206,214,0,103.1,112.25]],  animations: {idle:[0,19, true], shoot:[20,26,'idle',0.8], sad:[27,37,'idle',1], happy:[38,46, true], gameover:[47,57,'die',1],die:[57]}});   //width:

  var bubbleSprite = new createjs.SpriteSheet({images: ["images/bubble.png"], frames: [[0,0,234,216,0,118.5,117.45],[234,0,234,216,0,118.5,117.45],[468,0,234,216,0,118.5,117.45],[702,0,234,216,0,118.5,117.45],[0,216,234,216,0,118.5,117.45],[0,432,234,216,0,118.5,117.45],[0,648,234,216,0,118.5,117.45],[234,216,234,216,0,118.5,117.45],[468,216,234,216,0,118.5,117.45],[702,216,234,216,0,118.5,117.45],[234,432,234,216,0,118.5,117.45],[234,648,234,216,0,118.5,117.45],[468,432,234,216,0,118.5,117.45],[702,432,234,216,0,118.5,117.45]],animations:{
    'start':[0,3,'run',0.2],
    'run':[3],
    'bubble':[9],
    'pop':[9,13,'empty',0.2],
    'empty':[13]
  }});

  var dw = {
    bitmapFill:function (obj,img,repeat,matrix,x,y,width,height){
      obj = new createjs.Shape();
      obj.graphics.beginBitmapFill(img,repeat,matrix).drawRect(0,0,width,height);
      obj.x = x;
      obj.y = y;
      stage.addChild(obj);
      return obj;
    },
    sprite:function (obj,spriteSheet,status,x,y){
      obj = new createjs.Sprite(spriteSheet,status);
      obj.x = x;
      obj.y = y;
      stage.addChild(obj);
      return obj;
    },
    mkBubble:function (obj,spriteSheet,status,x,y){
      obj = new createjs.Sprite(spriteSheet,status);
      obj.x = x;
      obj.y = y;
      stage.addChild(obj);
      return obj;
    },
    mkObs:function (obj,img,x,y,score,impact,bonus,surround){
      obj = new createjs.Bitmap(queue.getResult(img));
      obj.x = x;
      obj.y = y;
      obj.regX = 25;
      obj.regY = 25;
      obj.setBounds(-25,-25,50,50);
      obj.score = score;
      obj.impact = impact;
      obj.surround = surround;
      obj.bonus = bonus;
      stage.addChild(obj);
      return obj;
    },
    'text':function (obj,text,font,color,textAlign,textBaseline,x,y){
      obj = new createjs.Text();
      obj.text = text;
      obj.font = font;
      obj.color = color;
      obj.textAlign = textAlign;
      obj.textBaseline = textBaseline;
      obj.x = x;
      obj.y = y;
      stage.addChild(obj);
      return obj;
    }
  };

  var mod = {
    'text':function (obj,text){
      obj.text = text;
    }
  };
  var game = {
    'loading':function (){    //游戏载入函数
      queue.addEventListener('progress',progress);
      function progress(event){    //preloading
        var proNum = Math.floor(event.progress*99);
        var num = document.getElementById('num');
        var bar = document.getElementById('bar');
        num.innerHTML = proNum + '%';
        bar.style.width = proNum + '%';
      };
    },
    'start': function (){    //游戏运行函数
      queue.addEventListener('complete',complete);
      function complete(){
        queue.removeEventListener('progress',progress);
        preload.style.display = 'none';
        createjs.Sound.play('menu');
        startView = dw.bitmapFill(startView,queue.getResult('startView'),'no-repeat',null,0,0,1008,640);  //绘制游戏开始的背景
        startView.addEventListener('click',function start(event){    //点击屏幕就开始游戏
          stage.removeChild(startView);
          event.target.removeEventListener('click',start);
          game.run();    //游戏运行
        });
      };
    },
    'run':function (){
      //createjs.Sound.play('theme',{loop:-1});
      bg = dw.bitmapFill(bg,queue.getResult('bg'),'no-repeat',null,0,0,1008,640);  //绘制游戏运行的背景
      user = dw.sprite(user,userSprite,'idle',1008/2,500);
      //apple = dw.mkObs(apple,'apple',504,100,null,null);
      //redBerry = dw.mkObs(redBerry,'redBerry',504,100,null,null);
      blueBerry = dw.mkObs(blueBerry,'blueBerry',504,-400,1,false,false,false);
      scoreTxt = dw.text(scoreTxt,'0分','52px Arial','blue','right','top',(1008-15),15);
      console.log(blueBerry.impact,blueBerry.bonus);
      bg.addEventListener('click',game.play);
      for(var i = 0;i<100;i++){
        xp[i] = Math.floor(Math.random()*1008);
      };
      game.down(blueBerry);
      gameRunInterval = setInterval(gameRun,30);
      function gameRun(){
        game.point(user,206,214);
        game.point(blueBerry,50,50);
        game.die(user,blueBerry);
        for(var j in bubbles){
          if(bubbles[j]){
            game.point(bubbles[j],234,216);
            //game.impact(bubbles[j],apple);
            //game.impact(bubbles[j],redBerry);
            game.impact(bubbles[j],blueBerry);
            game.addScore(bubbles[j],blueBerry);
          }
        }
      };
    },
    'point':function (obj,width,height){    //给所有的碰撞物与加分物进行坐标的赋值,并给对象的碰撞状态初始化
      //obj.impact = false;
      //console.log(obj.getBounds());
      var s = obj.getBounds();
      obj.minX = obj.x;
      obj.minY = obj.y;
      obj.maxX = obj.x + width;
      obj.maxY = obj.y + height;
    },
    'impact':function (obj1,obj2){
      if(obj1.maxX < obj2.minX || obj1.minX > obj2.maxX || obj1.maxY < obj2.minY || obj1.minY > obj2.maxY){
        obj2.impact = false;
        obj2.bonus = false;
        obj2.surround = false;
        return false;
     }else{
        obj2.impact = true;
        //game.surround(obj1,obj2);
     }
    },
    'die':function (obj1,obj2){
      if(obj1.maxX < obj2.minX || obj1.minX > obj2.maxX || obj1.maxY < obj2.minY || obj1.minY > obj2.maxY){
        return false;
     }else{
      user.gotoAndPlay('gameover');
      setTimeout(game.endView,1000);
     }
    },
    'endView':function (){
      clearInterval(downInterval);
      clearInterval(gameRunInterval);
      var bg = new createjs.Shape();
      bg.graphics.beginFill('black').drawRect(0,0,1008,640);
      bg.alpha = 0.5;
      stage.addChild(bg);
      var gameOver = dw.bitmapFill(gameOver,queue.getResult('gameOver'),'no-repeat',null,0,0,695,640);  //绘制游戏运行的背景
      gameOver.regX = 695/2;
      gameOver.regY = 640/2;
      gameOver.scaleX = gameOver.scaleY = 0;
      gameOver.x = 1008/2;
      gameOver.y = 640/2;
      createjs.Tween.get(gameOver).to({
        'scaleX':0.8,
        'scaleY':0.8
      },1000,createjs.Ease.bounceInOut).call(function (){
        bg.removeEventListener('click',game.play);
        bg.addEventListener('click',function (){
          window.location.reload();
        });
      });
      //stage.removeAllChildren();
    },
    'surround':function (bubble,obj){
      createjs.Tween.removeTweens(bubble);
      createjs.Tween.removeTweens(obj);
      bubble.gotoAndPlay('bubble');
      console.log(obj.x,obj.y);
      var a = new createjs.Point(bubble.x,bubble.y);
      var b = new createjs.Point(bubble.x-50,bubble.y-200);
      var c = new createjs.Point(bubble.x+50,bubble.y-400);
      var d = new createjs.Point(bubble.x,-400);
      var time = 400;

      createjs.Tween.get(obj).to({
        'x':a.x,
        'y':a.y
      },10).to({
        'x':b.x,
        'y':b.y
      },time).to({
        'x':c.x,
        'y':c.y
      },time).to({
        'x':d.x,
        'y':d.y
      },time);

      createjs.Tween.get(bubble).to({
        'x':a.x,
        'y':a.y
      },10).to({
        'x':b.x,
        'y':b.y
      },time).to({
        'x':c.x,
        'y':c.y
      },time).to({
        'x':d.x,
        'y':d.y
      },time).call(function (){
        stage.removeChild(bubble);
        bubbles[bubbles.indexOf(bubble)]=null;
      });

      bubble.surround = true;
      obj.surround = true;
    },
    'addScore':function (obj1,obj2){
      if(obj2.impact == true && obj2.bonus == false && obj2.surround == false){
        score+=obj2.score;
        game.surround(obj1,obj2);
        createjs.Sound.play('bubbled');
        obj2.bonus = true;
        console.log(score,obj2.impact,obj2.bonus);
        mod.text(scoreTxt,score + '分');
      }else{
        return false;
      };
    },
    'down':function (obj){
      var x = Math.floor(Math.random()*900-54);
      var a = new createjs.Point(504,-400);
      var b = new createjs.Point(x,640);
      downInterval = setInterval(goDown,2100);
      function goDown(){
      x = Math.floor(Math.random()*900-54);
      b = new createjs.Point(x,640);
      createjs.Tween.get(obj).to({'guide':{'path':[a.x,a.y,b.x,a.y,b.x,b.y]}},2000);
      };
    },
    'play':function (event){
      user.gotoAndPlay('shoot');
      bubbleIndex+=1;
      var a = new createjs.Point(504,500);
      var b = new createjs.Point(stage.mouseX,stage.mouseY);
      bubbles[bubbleIndex] = dw.mkBubble(bubbles[bubbleIndex],bubbleSprite,'start',a.x,a.y);
      game.goto(bubbles[bubbleIndex],a,b);
    },
    'goto':function(obj,a,b){
      createjs.Tween.get(obj).to({
        'x':b.x,
        'y':b.y
      },500).call(function (){
        createjs.Sound.stop('pop');
        createjs.Sound.play('pop');
        obj.gotoAndPlay('pop');
      }).wait(300).call(function (){
        stage.removeChild(obj);
        bubbles[bubbles.indexOf(obj)]=null;
      });
    }
  };

  game.loading();    //游戏开始载入
  game.start();   //游戏运行

  /*舞台刷新*/
  createjs.Ticker.setFPS(60);     //FPS update
  createjs.Ticker.addEventListener('tick',handleTicker);
  function handleTicker(){
    stage.update();
  };
};
