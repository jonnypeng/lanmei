var manifest = [
		{src:'images/apple.png',id:'apple'},
		{src:'images/bg.jpg',id:'bg'},
		{src:'images/blueBerry.png',id:'blueBerry'},
		{src:'images/redBerry.png',id:'redBerry'},
		{src:'images/bubble.png',id:'bubble'},
		{src:'images/user.png',id:'user'},
		{src:'images/startView.jpg',id:'startView'},
		{src:'images/endView.jpg',id:'endView'},
		{src:'images/GameOver.png',id:'gameOver'},
		{src:"sounds/bubbled.mp3", id:"bubbled"},
		{src:"sounds/die.mp3", id:"die"},
		{src:"sounds/score.mp3", id:"score"},
		{src:"sounds/pop.mp3", id:"pop"},
		{src:"sounds/menu.mp3", id:"menu"},
		{src:"sounds/theme.mp3", id:"theme"}
];
