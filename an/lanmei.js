(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != null && cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != null && cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != null && cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAFCiQgDgDgBgEQgEgSgCgaQgBgYAAgiIgBgHIgBgGQgBgDAEgBQADgDAFAAQAFAAAGADIAvgHQAXgDATAAQAKAAAMAFQALAEAGAHQAFAHAAAIIgBA1IgCAfIACAFQAAALgKABIgOgCIgNgBIgiACQgXABgQAEIgGACIgFABIgHACQgJAAgEgFgAA+A3QgRADgRAEIgEACIAAAAQAAAoACAQQABAMACAFIAQgCIAbgDIAbgCIAMgCIABgIQAEgfAAgdQgBgIgRAAQgSAAgSADgAiVCeQgFgJAAgGIAXgYQALgNAKgOQgQgSgHgPQgJgRAAgOQAAgQAJgjIAIgiIgVAIQgGAAgFgEQgEgGAAgKIABgHIACgCQAYgIASgEIAEgMIAOgtIABgKQAAgJAJAAQAFAAAGADIALAHQAFAEAAAHQAAADgDAEIgEAGQgGALgKAaIAOgBQAKAAAKAEQAJAFAHAIQAGAKABAOQgDAigEASQgLAkgJATIgRAlQAKAIAJACQAIADAEAAIAJAAQAGACADAFQADAEABAHQgBAHgEAEQgFAEgIAAQgLAAgRgIQgKgFgMgJQgPAYgMAOIgOAOIgHACQgIAAgHgJgAhdhEQgNAlgEAZQgGAaAAAJQAAAKALARIAJANIAMgYQANgeAIgYQAGgbAAgVQABgIgFgDQgFgCgFAAQgJAAgNACgAASgMQgLgDgGgFQgGgFgDgHQgEgHAAgKQAAgLAGgPQAGgQAMgTQAQgXASgXIACgHQADgCAGAAQAFAAAIAFQAKAIAAAJQAAAEgIAGQgLAHgVAcQgLAQgFALQgFAMAAAIQAAAPAYAAQAQAAASgFQASgGARgKIgBgDQgGgPgBgKQAAgHAEgEQACgCAGgCQAGAAAHAEQAFADABAGIACANQADAMAGAKIAKAPIAEAEIACAFQAAADgFAFQgFAFgGAAQgGAAgLgOIgBgEQgQAKgUAFQgiAIgRAAQgNAAgKgCg");
	this.shape.setTransform(63.7,2.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AAtCdQgGgHAAgNIAAgcIABg9IAAgyIgbABIhFAEQgFAkgDAiQgDAhABAfIABAGIACAIQAAAJgLAAQgJAAgEgGQgHgFgBgIQgBgIAAgSQAChAAHgsIgVADIgSADQgGACgIAAQgHAAgFgJQgGgIABgHQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABAAIBHgIIAQhZIACgMIgMACIgkAEQgGAAgDgDQgEgEgCgEQgCgEAAgFQAAgGAIgGIADABIAFAAIAGABIBbgHQBZgFAPgDIAWgDQAHAAAEADIAKAHQAFAEgBAHQAAAFgDAEQgEAEgGAAIgHgBIgIAAIgKgBIgUABQgDAIgBAMIgBAgIgBAvIAkgCIAWgDQAFAAAOAIQANAIAAAGQAAAMgMABQgIAAgHgCIgPgCIgwABIgBAwQAAAcADAxIAEAQIACAIIABAHQAAAGgEADQgEADgFAAQgOAAgGgHgAAJh/IgmADIgCAFQgEAHgCAKQgJApgGAmIBMgFIAQgBIAAgeIgBguIgCgQIgCgHg");
	this.shape_1.setTransform(17.9,2.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#333333").s().p("ABhCmQgGgHAAgLIAAhPIgBg8IgBgSIAAgCIgkACIgFgBQgDA7AAAlIACAUQACANAEAIIADAIQAAAGgEADQgEADgGABQgHAAgGgDQgFgCgDgGQgEgFgBgMIgBgfQADhfAIhTIABgEQgHgDgDgEQgDgEAAgFQAAgKAFgIIAMgCIAmgOIAlgNQAPgFAFAAQAPAGACAJQgBAMgCABIgMAEQgoALgaAMIgCAHQgEAKgEAwIgBAEQASAAAegFQAhgFANAAQAIAAAGAGQAFAGAAAGQAAAHgEADQgDAGgIAAIgPgCIgKABIAAADQgCAKAAARIgBAxIABA1QAAAPAEATIABAIQABAGgEADQgEADgEgBQgNAAgGgFgAhPCmQgEgEgCgUQgCgVAAglIAAgmQgZACgUAEIgIACIgFACQgIAAgFgJQgFgKAAgFQAAgBAAgBQAAAAABgBQAAAAABgBQAAAAABAAIArgGIAdgDIgBgeIAAgFQgVACgUAFIgLACQgFAAgEgHQgDgHAAgIQAAgFAEgCIAggEQgGgRgDgNQgDgPAAgJIgMABQgEAAgEgEQgEgDgCgFQgBgDAAgGQgBgFAIgHIADABIAFABIAHAAIAsgDIgHgJIgFgGIgEgBQgBgBgBgGQAAgHAGgHQAEgGAJgBQAEABAGAGQAHAHAHAMIAGAQIAcgDIAMgCQAFAAAFACIAJAHQAFAEAAAHQgBAGgEADQgDAEgGABIgGgBIgDAAIAAAAIgFAHQgGAUgEAWIgBAGIAAAAIAUABQANABABAHIgBAPQgCAEgFAAIgMAAIgMAAIgWAAIgCAJQgBAGAAATIAMgBIAVgDQAGAAAMAHQANAKAAAFQAAAMgKAAQgJAAgGgCIgPgBIgYABIAAArIABAjQABANADAHQAAAMgEAIQgEAJgGAAQgIAAgFgFgAhNhkIgXACIABAMQgBAKADAOIAFARIABACIADAAIAcgCIADgTQAEgUAGgRgAiTCPQgGgGAAgHQAAgGAFgLQAKgVALgOQAFgHAFgCIAGgBQAEAAAFACQADAFABAEIABADIgCAGIgFAFQgKAPgFASQgDAIAAAIIAAAHIgFABQgPgBgFgGgAgUB5IgHgLIgMgPIgEgEIgDgDQgEgEAAgEQABgEAEgGQAFgHAEAAQADAAAFAEIAGAGIAMANIAIAKIACABIAAABQABAJgCAFQgDAGgEADQgEACgGAAg");
	this.shape_2.setTransform(-27.8,2.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#333333").s().p("ACDCgQgLgDgugBIghAAQhbABgXACQgkAEgMABQgMgBgHgEQgHgEgDgIIAJgDIAMgEQAkgFBMgFIAAgFIAAgSIg5AFIgjADQgFAAgEgEQgDgEgCgDQgBgDgBgGQAAgGAHgEIADAAIAGABIAGAAIBWgGIABgXIgyAEIgMACIgBABIgBACQgFADgGAAQgIAAgDgFQgEgCgBgEQgFgSgCgYQgCgXAAggIgBgHIgBgFQAAgDAEgCQADgCAGAAQAGAAAIAEIABABQAfgEArgFIAAgUQg/AHgYADIgRAEQgGACgHAAQgHAAgFgKQgGgIAAgHQAAgBAAgBQAAAAABgBQAAAAABAAQABAAABAAQAwgHArgDIAmgEIgBgOIgBgEIg+AHQgQADgQAAQgEAAgDgDQgEgEAAgGIABgOIAHgBIAsgDIBCgIIBDgHIAogDQAHAAAFAGQADAEAAAGQAAAFgCADIgGADIgiACIg3AFQgCAIAAALIgBACIAHAAIBNgFIAVgDQAFAAANAHQANAJAAAGQAAAMgLAAQgJAAgGgCIgPgBQgVAAhKADIAAACIgBASQAggCAcAAQAKABAMADQAKAFAGAFQAGAGAAAHQAAAZgEAZIgEAaQAFAEAAADQAAAMgKAAIgOgCIgNgBIhBACIABAYQA4gDALgCIAWgCQAFAAAFACIAKAGQADAFAAAGQAAAGgDADQgDAEgHAAIgGAAIgJgBIgJAAIhKACIAAAGIADAPIABACIBHgCQAbAAARAGQAJADAFAFQAEAGAAAJQAAALgKAAgAhKAPIgJABIABAIIABAOIAsgEIAVgBIAAgZgAANAGIAAAZIADAAIA0gDIAKgCIABgGIADgQIgJAAQgUAAgoACgAhIgfIgMACIAAABIAAAAIAAAXIACAAIAzgFIAPgBIAAgcgAAOgpIgBAcIBCgFIAHgBIABgRQgBgHgSAAg");
	this.shape_3.setTransform(-74.1,2.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCCCCC").s().p("AvnH0IAAvnIfPAAIAAPng");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-100,-50,200,100);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AAFCiQgDgDgBgEQgEgSgCgaQgBgYAAgiIgBgHIgBgGQgBgDAEgBQADgDAFAAQAFAAAGADIAvgHQAXgDATAAQAKAAAMAFQALAEAGAHQAFAHAAAIIgBA1IgCAfIACAFQAAALgKABIgOgCIgNgBIgiACQgXABgQAEIgGACIgFABIgHACQgJAAgEgFgAA+A3QgRADgRAEIgEACIAAAAQAAAoACAQQABAMACAFIAQgCIAbgDIAbgCIAMgCIABgIQAEgfAAgdQgBgIgRAAQgSAAgSADgAiVCeQgFgJAAgGIAXgYQALgNAKgOQgQgSgHgPQgJgRAAgOQAAgQAJgjIAIgiIgVAIQgGAAgFgEQgEgGAAgKIABgHIACgCQAYgIASgEIAEgMIAOgtIABgKQAAgJAJAAQAFAAAGADIALAHQAFAEAAAHQAAADgDAEIgEAGQgGALgKAaIAOgBQAKAAAKAEQAJAFAHAIQAGAKABAOQgDAigEASQgLAkgJATIgRAlQAKAIAJACQAIADAEAAIAJAAQAGACADAFQADAEABAHQgBAHgEAEQgFAEgIAAQgLAAgRgIQgKgFgMgJQgPAYgMAOIgOAOIgHACQgIAAgHgJgAhdhEQgNAlgEAZQgGAaAAAJQAAAKALARIAJANIAMgYQANgeAIgYQAGgbAAgVQABgIgFgDQgFgCgFAAQgJAAgNACgAASgMQgLgDgGgFQgGgFgDgHQgEgHAAgKQAAgLAGgPQAGgQAMgTQAQgXASgXIACgHQADgCAGAAQAFAAAIAFQAKAIAAAJQAAAEgIAGQgLAHgVAcQgLAQgFALQgFAMAAAIQAAAPAYAAQAQAAASgFQASgGARgKIgBgDQgGgPgBgKQAAgHAEgEQACgCAGgCQAGAAAHAEQAFADABAGIACANQADAMAGAKIAKAPIAEAEIACAFQAAADgFAFQgFAFgGAAQgGAAgLgOIgBgEQgQAKgUAFQgiAIgRAAQgNAAgKgCg");
	this.shape.setTransform(63.7,2.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AAtCdQgGgHAAgNIAAgcIABg9IAAgyIgbABIhFAEQgFAkgDAiQgDAhABAfIABAGIACAIQAAAJgLAAQgJAAgEgGQgHgFgBgIQgBgIAAgSQAChAAHgsIgVADIgSADQgGACgIAAQgHAAgFgJQgGgIABgHQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABAAIBHgIIAQhZIACgMIgMACIgkAEQgGAAgDgDQgEgEgCgEQgCgEAAgFQAAgGAIgGIADABIAFAAIAGABIBbgHQBZgFAPgDIAWgDQAHAAAEADIAKAHQAFAEgBAHQAAAFgDAEQgEAEgGAAIgHgBIgIAAIgKgBIgUABQgDAIgBAMIgBAgIgBAvIAkgCIAWgDQAFAAAOAIQANAIAAAGQAAAMgMABQgIAAgHgCIgPgCIgwABIgBAwQAAAcADAxIAEAQIACAIIABAHQAAAGgEADQgEADgFAAQgOAAgGgHgAAJh/IgmADIgCAFQgEAHgCAKQgJApgGAmIBMgFIAQgBIAAgeIgBguIgCgQIgCgHg");
	this.shape_1.setTransform(17.9,2.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#333333").s().p("ABhCmQgGgHAAgLIAAhPIgBg8IgBgSIAAgCIgkACIgFgBQgDA7AAAlIACAUQACANAEAIIADAIQAAAGgEADQgEADgGABQgHAAgGgDQgFgCgDgGQgEgFgBgMIgBgfQADhfAIhTIABgEQgHgDgDgEQgDgEAAgFQAAgKAFgIIAMgCIAmgOIAlgNQAPgFAFAAQAPAGACAJQgBAMgCABIgMAEQgoALgaAMIgCAHQgEAKgEAwIgBAEQASAAAegFQAhgFANAAQAIAAAGAGQAFAGAAAGQAAAHgEADQgDAGgIAAIgPgCIgKABIAAADQgCAKAAARIgBAxIABA1QAAAPAEATIABAIQABAGgEADQgEADgEgBQgNAAgGgFgAhPCmQgEgEgCgUQgCgVAAglIAAgmQgZACgUAEIgIACIgFACQgIAAgFgJQgFgKAAgFQAAgBAAgBQAAAAABgBQAAAAABgBQAAAAABAAIArgGIAdgDIgBgeIAAgFQgVACgUAFIgLACQgFAAgEgHQgDgHAAgIQAAgFAEgCIAggEQgGgRgDgNQgDgPAAgJIgMABQgEAAgEgEQgEgDgCgFQgBgDAAgGQgBgFAIgHIADABIAFABIAHAAIAsgDIgHgJIgFgGIgEgBQgBgBgBgGQAAgHAGgHQAEgGAJgBQAEABAGAGQAHAHAHAMIAGAQIAcgDIAMgCQAFAAAFACIAJAHQAFAEAAAHQgBAGgEADQgDAEgGABIgGgBIgDAAIAAAAIgFAHQgGAUgEAWIgBAGIAAAAIAUABQANABABAHIgBAPQgCAEgFAAIgMAAIgMAAIgWAAIgCAJQgBAGAAATIAMgBIAVgDQAGAAAMAHQANAKAAAFQAAAMgKAAQgJAAgGgCIgPgBIgYABIAAArIABAjQABANADAHQAAAMgEAIQgEAJgGAAQgIAAgFgFgAhNhkIgXACIABAMQgBAKADAOIAFARIABACIADAAIAcgCIADgTQAEgUAGgRgAiTCPQgGgGAAgHQAAgGAFgLQAKgVALgOQAFgHAFgCIAGgBQAEAAAFACQADAFABAEIABADIgCAGIgFAFQgKAPgFASQgDAIAAAIIAAAHIgFABQgPgBgFgGgAgUB5IgHgLIgMgPIgEgEIgDgDQgEgEAAgEQABgEAEgGQAFgHAEAAQADAAAFAEIAGAGIAMANIAIAKIACABIAAABQABAJgCAFQgDAGgEADQgEACgGAAg");
	this.shape_2.setTransform(-27.8,2.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#333333").s().p("ACDCgQgLgDgugBIghAAQhbABgXACQgkAEgMABQgMgBgHgEQgHgEgDgIIAJgDIAMgEQAkgFBMgFIAAgFIAAgSIg5AFIgjADQgFAAgEgEQgDgEgCgDQgBgDgBgGQAAgGAHgEIADAAIAGABIAGAAIBWgGIABgXIgyAEIgMACIgBABIgBACQgFADgGAAQgIAAgDgFQgEgCgBgEQgFgSgCgYQgCgXAAggIgBgHIgBgFQAAgDAEgCQADgCAGAAQAGAAAIAEIABABQAfgEArgFIAAgUQg/AHgYADIgRAEQgGACgHAAQgHAAgFgKQgGgIAAgHQAAgBAAgBQAAAAABgBQAAAAABAAQABAAABAAQAwgHArgDIAmgEIgBgOIgBgEIg+AHQgQADgQAAQgEAAgDgDQgEgEAAgGIABgOIAHgBIAsgDIBCgIIBDgHIAogDQAHAAAFAGQADAEAAAGQAAAFgCADIgGADIgiACIg3AFQgCAIAAALIgBACIAHAAIBNgFIAVgDQAFAAANAHQANAJAAAGQAAAMgLAAQgJAAgGgCIgPgBQgVAAhKADIAAACIgBASQAggCAcAAQAKABAMADQAKAFAGAFQAGAGAAAHQAAAZgEAZIgEAaQAFAEAAADQAAAMgKAAIgOgCIgNgBIhBACIABAYQA4gDALgCIAWgCQAFAAAFACIAKAGQADAFAAAGQAAAGgDADQgDAEgHAAIgGAAIgJgBIgJAAIhKACIAAAGIADAPIABACIBHgCQAbAAARAGQAJADAFAFQAEAGAAAJQAAALgKAAgAhKAPIgJABIABAIIABAOIAsgEIAVgBIAAgZgAANAGIAAAZIADAAIA0gDIAKgCIABgGIADgQIgJAAQgUAAgoACgAhIgfIgMACIAAABIAAAAIAAAXIACAAIAzgFIAPgBIAAgcgAAOgpIgBAcIBCgFIAHgBIABgRQgBgHgSAAg");
	this.shape_3.setTransform(-74.1,2.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCCCCC").s().p("AvnH0IAAvnIfPAAIAAPng");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-100,-50,200,100);


(lib.button = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AB9CeQgWgSgKgNQgTgcgIgSIgFAHQgkAtgJASIgHAJQgFAFgHAAQgHAAgEgHQgFgHAAgJIABgFIAIgLIAeglQAKgNAXgaQgIgUgFgZQgGgZgEgfIgSAFIgRAFQgEADgIAAQgHgCgDgEQgDgEAAgGQAAgKAFgHIAQgCIAlgHIgDggIgBggQAAgUAOgBQAHAAAFACQAKAGAAAJQgEAEAAAFIAAAbIABAZIAngHQAUgEAJAAQALAGACAHQAAALgCABIgRADQgYAEgjAIQAJA7AHAZQAagaAKgHQAPgKAKAAQANABAAAJQAAAHgFAEQgGADgUAQQgLAJgVAYQAJAXAUAfQAHAKAHAHQAHAGAHADIAHgBQAJAHAAALQAAAGgEAEQgEADgHAAQgJAAgLgIgAiWCNQgFgHgBgOIALgKQAUglATgoIAFgLQgOgTgQgOQgIgJgKgGIgGgDQgDgCAAgEQAAgEAEgGQAEgGAHAAQAJAAALAIQAMAIAXAZQAJgXAJgcQAFgTABgQQAAgJgKAAQgQAAgOAEQgPADgOAIIgJAEQgIAAgEgGQgEgFgBgLIABgDQAagKAUgFQAVgFAPAAQAQAAAIAGQAJAFADAHQADAIAAAMQAAAZgPAwIgOAlQAVAiAJAUQAHAQAAALQAAAIgGAGQgGAFgGAAQgLgCAAgFQAAgHgIgVIgPgcIgGAMIghBDQgKAQgHAAQgIgBgFgHgACEhrIgJgKIgPgKIgLgFIgFgBQgCgBAAgEQAAgHAFgGQAEgGAIAAQAFAAAJAGQAJAFALALQAKAMAAAEQAAAEgEAFQgDAEgJABQgBAAAAAAQgBAAAAAAQAAgBAAAAQgBgBAAAAg");
	this.shape.setTransform(164,52);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AgUChQgIgHAAgFIABgEIADgDQAHgSAHghQAEgYADguQAAgMgOAAQgGAAgGAEIgDABIgMBNIgIA0QgDAJgIAAQgFAAgGgIQgHgJAAgFIABgIIADgMIALg6IAWiBIAAgEIgKACIgLADQgFAAgDgEQgEgDgCgEQgBgDgBgGQAAgFAIgGIACABIAGABIAGAAIAcgFIAFAAIgGgJIgKgKIgIgFQgFgDAAgGQgBgHAFgGQAFgHAJAAQAIABAMARQAMARABAGQAAAEgEAGIAIgCIAIgCQAFAAAFACIAJAGQAEAEABAHQgBAFgEAEQgDAEgGAAIgGgBIgIAAIgJAAIgGAAIgBAFIgEAKQgDAPgIAqIALgBQAKAAAHADQAIADAIAHQAGAHABAOQgDA7gEAaQgIAngHAQQgFAIgFAAQgEAAgIgHgABBCfQgHgHgBgHQAAgJAJAAIAFABQADACABAEQADAFAFAAQAEAAABgGQACgGAAgUQAAgVgFgiIgWAGIgLAEIgFACQgHAAgFgIQgFgIAAgFQAAgFAEgCIAGgCIAbgEIANgDIgBgIIgDgLQAAgJADgDQADgDAGgBQAFgDAJgLQAKgLgBgJQAAgDgGAAQgKAAgLADQgKADgLAFIgIAEQgGAAgEgGQgDgGAAgKIABgCQASgJAQgEQAQgEAOAAQANAAAJAFQAJAGADAFQACAHAAAEQAAAJgGANQgHAMgMAOIgJAIIACALIANgCIAGgBQAJAAALAGQAKAHABAGQgBAMgKAAIgPgCIgRgCIgDAAQAFAhgBAYQAAANgCAKQgBAKgFAGQgDAHgGADQgGADgHAAQgQAAgIgGgAiYCWQgFgEABgIQAAgKACgHIAKgFIASgNIAUgNQAIgFAEAAQAGAAAFAFIACAKIgBAGQgBAEgFACIgJAFQgGAEgMALIgRAQQgFAIgEAAQgHgBgEgFgAhsAAIgLgHQgJgEgKAAIgJAAQgFAAgEgEQgDgDgBgKQAAgJAGgDQADgEAIAAQAGAAAOAEQAMAEAKAHQAIAIABAFQAAAGgDAIQgCAGgEAAQgDAAgEgEgAAkhBQgFgEAAgIQAAgHAGgRQAOghALgTQAHgKAFgCIAHgCQAFAAAEADQADAEACAEIABAEQAAADgDACQgDACgDAGIgMAUIAYgEIAfgEIAGgBQAFAAAFACIAKAGQADAEABAGQAAAGgEADQgEAEgFAAIgGgBIgIAAIgJAAIgXABIgXAEIgKACIgDAAIgBACQgDAMAAALIAAAHIgGABQgNAAgGgHgAhhhuIgLgHQgHgFgKAAIgIABQgEAAgFgEQgDgEAAgJQAAgJAEgDQAFgDAHAAQAHAAALADQAMAFAJAGQAIAHAAAGQAAAGgCAHQgDAHgEAAQgDAAgDgEg");
	this.shape_1.setTransform(118,52.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#333333").s().p("AAFCiQgDgDgBgEQgEgSgCgaQgBgYAAgiIgBgHIgBgGQAAgDADgBQAEgDAEAAQAFAAAGADIAvgHQAXgDATAAQAKAAAMAFQAKAEAHAHQAFAHABAIIgCA1IgCAfIACAFQAAALgKABIgOgCIgNgBIgiACQgXABgQAEIgGACIgFABIgHACQgJAAgEgFgAA+A3QgRADgRAEIgEACIAAAAQAAAoACAQQABAMACAFIAQgCIAbgDIAbgCIAMgCIABgIQAEgfAAgdQgBgIgSAAQgRAAgSADgAiUCeQgGgJAAgGIAXgYQAMgNAJgOQgQgSgHgPQgJgRAAgOQAAgQAJgjIAIgiIgVAIQgHAAgEgEQgDgGgBgKIABgHIACgCQAYgIASgEIAEgMIAOgtIABgKQAAgJAJAAQAFAAAHADIAKAHQAFAEAAAHQAAADgDAEIgEAGQgGALgKAaIAOgBQAJAAALAEQAJAFAHAIQAGAKAAAOQgCAigEASQgLAkgJATIgRAlQAKAIAJACQAIADAEAAIAJAAQAGACADAFQADAEABAHQgBAHgEAEQgFAEgIAAQgLAAgRgIQgKgFgMgJQgPAYgMAOIgOAOIgHACQgJAAgFgJgAhdhEQgNAlgEAZQgGAaAAAJQAAAKALARIAJANIAMgYQAOgeAGgYQAIgbgBgVQABgIgFgDQgEgCgGAAQgJAAgNACgAASgMQgKgDgHgFQgGgFgDgHQgEgHAAgKQAAgLAGgPQAGgQAMgTQAQgXASgXIACgHQADgCAGAAQAFAAAIAFQAKAIgBAJQABAEgIAGQgKAHgWAcQgKAQgGALQgFAMAAAIQAAAPAYAAQAQAAASgFQASgGARgKIgBgDQgHgPAAgKQAAgHAEgEQACgCAGgCQAGAAAHAEQAEADACAGIACANQADAMAGAKIAKAPIAEAEIACAFQAAADgFAFQgFAFgFAAQgHAAgLgOIgBgEQgQAKgUAFQgiAIgRAAQgNAAgKgCg");
	this.shape_2.setTransform(71.7,52.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#333333").s().p("AAtCdQgGgHAAgNIAAgcIABg9IAAgyIgbABIhFAEQgFAkgDAiQgDAhABAfIACAGIABAIQAAAJgLAAQgJAAgFgGQgGgFgBgIQgCgIAAgSQAEhAAFgsIgUADIgSADQgGACgIAAQgGAAgGgJQgGgIABgHQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABAAIBIgIIAPhZIACgMIgNACIgkAEQgEAAgFgDQgEgEgBgEQgCgEAAgFQAAgGAHgGIAEABIAFAAIAGABIBbgHQBZgFAQgDIAVgDQAHAAAEADIAKAHQAEAEAAAHQAAAFgDAEQgEAEgGAAIgHgBIgIAAIgJgBIgVABQgDAIgBAMIgBAgIgBAvIAlgCIAVgDQAFAAANAIQANAIABAGQAAAMgMABQgJAAgGgCIgPgCIgwABIgBAwQAAAcADAxIAEAQIACAIIACAHQgBAGgEADQgEADgGAAQgNAAgGgHgAAJh/IgmADIgCAFQgEAHgCAKQgJApgGAmIBMgFIAQgBIAAgeIgBguIgCgQIgCgHg");
	this.shape_3.setTransform(25.9,52.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#CCCCCC").s().p("AvnH0IAAvnIfPAAIAAPng");
	this.shape_4.setTransform(100,50);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#333333").s().p("AAFCiQgDgDgBgEQgDgSgDgaQgCgYAAgiIAAgHIgBgGQgBgDAEgBQADgDAFAAQAFAAAGADIAvgHQAXgDATAAQAKAAAMAFQALAEAGAHQAGAHgBAIIgBA1IgCAfIACAFQAAALgKABIgOgCIgNgBIgiACQgYABgPAEIgGACIgFABIgHACQgJAAgEgFgAA+A3QgRADgRAEIgEACIAAAAQAAAoACAQQABAMACAFIAQgCIAbgDIAbgCIAMgCIABgIQAEgfAAgdQgBgIgRAAQgSAAgSADgAiVCeQgFgJAAgGIAXgYQALgNAKgOQgQgSgHgPQgJgRAAgOQAAgQAJgjIAIgiIgUAIQgHAAgFgEQgEgGAAgKIABgHIACgCQAYgIASgEIAEgMIAOgtIABgKQAAgJAJAAQAFAAAGADIALAHQAFAEAAAHQAAADgDAEIgEAGQgGALgKAaIAOgBQAJAAAKAEQAKAFAHAIQAGAKABAOQgDAigEASQgLAkgJATIgRAlQALAIAIACQAIADAEAAIAJAAQAGACADAFQAEAEAAAHQgBAHgFAEQgEAEgIAAQgLAAgRgIQgKgFgMgJQgPAYgMAOIgOAOIgHACQgIAAgHgJgAhdhEQgNAlgEAZQgGAaAAAJQAAAKALARIAJANIAMgYQANgeAIgYQAGgbAAgVQABgIgFgDQgFgCgFAAQgKAAgMACgAASgMQgKgDgHgFQgGgFgDgHQgEgHAAgKQAAgLAGgPQAGgQAMgTQAQgXASgXIABgHQAEgCAGAAQAGAAAHAFQAKAIAAAJQAAAEgIAGQgKAHgWAcQgKAQgFALQgGAMAAAIQAAAPAYAAQARAAARgFQARgGASgKIgBgDQgGgPgBgKQABgHADgEQACgCAGgCQAGAAAHAEQAFADABAGIACANQADAMAFAKIALAPIAEAEIACAFQAAADgFAFQgEAFgHAAQgGAAgLgOIgCgEQgOAKgVAFQgiAIgRAAQgNAAgKgCg");
	this.shape_5.setTransform(163.7,52.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#333333").s().p("AAtCdQgGgHAAgNIAAgcIABg9IAAgyIgbABIhFAEQgFAkgDAiQgCAhAAAfIABAGIACAIQAAAJgLAAQgIAAgFgGQgHgFgBgIQgBgIAAgSQAChAAHgsIgVADIgSADQgGACgIAAQgHAAgFgJQgFgIAAgHQAAgBAAAAQAAgBAAAAQAAgBABAAQAAAAABAAIBHgIIAQhZIACgMIgMACIgkAEQgGAAgDgDQgFgEgBgEQgCgEAAgFQAAgGAIgGIACABIAGAAIAGABIBbgHQBZgFAPgDIAXgDQAGAAAEADIAKAHQAFAEgBAHQAAAFgDAEQgEAEgGAAIgIgBIgHAAIgKgBIgUABQgDAIgBAMIgBAgIgBAvIAkgCIAWgDQAFAAAOAIQANAIgBAGQAAAMgLABQgIAAgHgCIgPgCIgwABIgBAwQAAAcADAxIAEAQIACAIIABAHQABAGgFADQgEADgFAAQgOAAgGgHgAAKh/IgnADIgCAFQgEAHgCAKQgJApgGAmIBMgFIAQgBIAAgeIgBguIgCgQIgCgHg");
	this.shape_6.setTransform(117.9,52.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#333333").s().p("ABhClQgGgFAAgNIAAhPIgBg6IgBgTIAAgCIgkACIgEgBQgEA7AAAkIACAVQADAOADAHIADAJQAAAFgEAEQgEACgGAAQgHABgGgDQgFgDgDgEQgDgGgCgMIgBgfQAEhfAHhSIABgGQgHgCgDgDQgDgFAAgGQAAgJAFgHIAMgDIAmgNIAlgPQAQgEAEAAQAOAHADAHQgBANgCACIgMADQgoALgbAMIgBAIQgDAJgFAwIgBAEQASAAAegFQAhgFANAAQAIAAAGAGQAFAGAAAFQAAAIgEADQgDAFgIAAIgQgBIgJABIAAADQgCAKAAASIgBAwIABA1QAAAOAEAVIABAHQABAFgEAEQgEACgFABQgMAAgGgHgAhPCmQgEgEgCgVQgCgUAAglIAAgmQgZACgUAEIgHADIgGABQgIAAgFgJQgFgJAAgHQAAAAAAgBQAAAAABgBQAAAAABgBQAAAAABAAIAqgHIAegCIgBgfIAAgDQgVACgUAEIgLACQgFAAgEgHQgDgHAAgIQAAgFAEgBIAggFQgGgRgDgMQgEgRABgIIgMABQgEAAgEgEQgEgDgCgFQgBgDAAgFQgBgGAIgGIADABIAFAAIAHABIAsgEIgHgIIgFgGIgEgCQgBgCgBgFQAAgIAGgGQAFgHAIABQAFgBAFAHQAGAGAIAOIAGAOIAcgCIAMgCQAFAAAFACIAJAHQAFAEAAAHQAAAGgFADQgCAFgHgBIgGgBIgEAAIAAABIgDAHQgIAUgDAWIgBAHIAAAAIAUAAQANAAABAIIgBAOQgCAFgFAAIgMAAIgMAAIgXAAIgBAJQgBAGAAATIAMgBIAVgDQAFAAANAIQANAJAAAFQAAAMgKAAQgJAAgHgBIgOgCIgYABIAAAqIABAjQABAOADAGQAAAMgEAKQgFAIgFAAQgIAAgFgFgAhMhjIgYABIAAAMQABAKACAOIAFASIABACIADgBIAcgBIADgUQAEgUAGgSgAiTCPQgGgFAAgJQAAgFAFgLQAKgVALgOQAFgHAEgCIAHgCQAFAAAEAEQADAEABAEIABAEIgCAEIgEAGQgLAOgFAUQgDAHAAAHIABAIIgGAAQgPABgFgHgAgUB4IgHgJIgMgQIgEgEIgDgEQgDgDAAgDQAAgFAEgHQAEgGAFAAQADAAAFAEIAGAGIAMANIAIAJIACACIAAACQABAHgCAGQgDAGgEADQgEACgGAAg");
	this.shape_7.setTransform(72.2,52.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#333333").s().p("ACDCgQgLgCgugCIghgBQhbABgXAEQgkADgMAAQgMAAgHgEQgHgEgDgIIAJgDIAMgEQAjgFBNgGIAAgDIAAgTIg5AEIgjAEQgFAAgEgEQgDgEgCgEQgCgDAAgFQABgFAGgGIADAAIAFABIAHAAIBWgEIABgZIgyAGIgLACIgCAAIgBABQgFAFgGAAQgHAAgEgGQgEgCgBgFQgFgQgCgZQgCgXAAgfIgBgIIgBgFQAAgDAEgCQAEgCAFAAQAGAAAIAEIABABQAfgFArgEIAAgTQhAAFgXAEIgRADQgGACgHAAQgHABgFgKQgGgIAAgHQAAgBAAgBQAAAAABgBQAAAAABAAQABgBABAAQAwgGArgEIAmgCIgBgPIgBgEIg+AIQgQADgPAAQgGgBgCgDQgEgEAAgGIABgOIAHgBIAsgEIBCgGIBDgHIAogEQAHAAAFAGQADAFAAAEQAAAFgCAEIgGACIgiADIg3AFQgCAIgBAKIAAAEIAHgBIBNgFIAVgDQAGAAAMAIQANAIAAAGQAAAMgLAAQgJAAgGgCIgOgBQgWAAhKAEIAAACIgBASQAfgCAeAAQAIAAANADQAKAFAGAFQAGAGAAAIQAAAYgEAZIgEAaQAFAFAAADQAAALgKAAIgOgCIgNAAIhBABIABAYQA5gDAKgCIAWgCQAFAAAFACIAKAHQADADAAAHQAAAGgDADQgDAEgHAAIgGgBIgJAAIgJgBIhKADIAAAFIADAQIABACIBHgCQAbABARAFQAJACAFAHQAEAFAAAIQAAAMgKAAgAhKAPIgJABIABAIIABANIAsgDIAVgBIAAgYgAANAHIAAAYIACAAIA1gDIAKgBIABgHIADgQIgJgBQgUAAgoAEgAhIgfIgMADIAAAAIAAABIAAAVIACAAIAzgEIAPgBIAAgbgAAOgpIgBAbIBCgDIAHgBIABgRQgCgIgRAAg");
	this.shape_8.setTransform(25.9,52.2);

	this.instance = new lib.Tween3("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(100,50);
	this.instance._off = true;

	this.instance_1 = new lib.Tween4("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(200,150);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_4},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance}]},1).to({state:[{t:this.instance_1}]},1).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(2).to({_off:false},0).wait(1).to({x:116.7,y:66.7},0).wait(1).to({x:133.4,y:83.4},0).wait(1).to({x:150,y:100},0).wait(1).to({x:166.7,y:116.7},0).wait(1).to({x:183.4,y:133.4},0).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,200,100);


// stage content:
(lib.lanmei = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.button();
	this.instance.parent = this;
	this.instance.setTransform(472.8,184.3,1,1,0,0,0,100,50);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(876.8,454.3,200,100);
// library properties:
lib.properties = {
	width: 1008,
	height: 640,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;